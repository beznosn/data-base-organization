/*  ****************************** LAB 3 ********************************************* */
use StudentsDataBase;


INSERT StudentTable (FirstName, SecondName, age, AVGGrade) VALUES ('Dan', 'Ddfj', 19, null);
INSERT StudentTable (FirstName, SecondName, age, AVGGrade) VALUES ('Danny', 'Ddgffj', 18, null);
INSERT StudentTable (FirstName, SecondName, age, AVGGrade) VALUES ('Donald', 'Gjddfj', 17, null);

INSERT GroupTable (GroupName, Course) VALUES ('newGroup1', 1);

INSERT SubjectTable (SubjectName, Semesters, Teacher, SubjectSpecialty, Classrooms ) VALUES ('Physics', 3, 1, 'Physics Specialty', 1);

INSERT TeacherTable (FirstName, SecondName, ExperienceYears, Ranks) VALUES ('Oleh', 'Ivanych' ,15, 'Shnobel prize owner, Just a good guy medal candidate');
INSERT TeacherTable (FirstName, SecondName, ExperienceYears, Ranks) VALUES ('Olena', 'Letvinchyc', 5, 'just a teacher');
INSERT TeacherTable (FirstName, SecondName, ExperienceYears, Ranks) VALUES ('Igor', 'Ivanov', 1, 'New fish');

INSERT ClassroomsTable (hasComputers, Adress) VALUES (true, 'Lybidska st., 42');
INSERT ClassroomsTable (hasComputers, Adress) VALUES (false, 'Lybidska st., 42');
INSERT ClassroomsTable (hasComputers, Adress) VALUES (true, 'Lybidska st., 42');



#--------------------------------- SELECT ---------------------------------------------
SELECT * FROM ClassroomsTable WHERE hasComputers = true;
SELECT * FROM StudentTable
ORDER BY age ASC; #--------------------------------- ORDER BY ----------------------




#--------------------------------- DELETE ---------------------------------------------
#ОТЧИСЛЕНИЕ ПОСЛЕ СЕССИИ
DELETE FROM StudentTable WHERE AVGGrade <= 60;




#--------------------------------- GROUP BY ---------------------------------------------


SELECT TeacherID, COUNT(*) AS ExperienceYears
FROM TeacherTable GROUP BY TeacherID;



#--------------------------------- Подзапросы + UPDATE -----------------------------------
UPDATE StudentTable SET age = age + 1
WHERE StudentId in (SELECT Id FROM StudentTable WHERE age < 18);
UPDATE ClassroomsTable SET hasComputers = true WHERE hasComputers = false;

create database  StudentsDataBase;
use StudentsDataBase;

create table StudentTable
(StudentId int PRIMARY KEY AUTO_INCREMENT,
FirstName VARCHAR(20),
SecondName VARCHAR(20),
age int,
GroupID int,
AVGGrade FLOAT,
Specialty VARCHAR(20),
FOREIGN KEY (GroupID) REFERENCES GroupTable (Students)

);

create table GroupTable
(GroupID int PRIMARY KEY AUTO_INCREMENT,
GroupName VARCHAR(20),
Course TINYINT UNSIGNED,
Students int,
FOREIGN KEY (GroupID) REFERENCES SubjectTable (SubjectID)
);

create table SubjectTable
(SubjectID int PRIMARY KEY AUTO_INCREMENT,
SubjectName VARCHAR(20),
Semesters TINYINT UNSIGNED,
Teacher int,
SubjectSpecialty VARCHAR(20),
Classrooms int,
FOREIGN KEY (Teacher) REFERENCES TeacherTable (TeacherID),
FOREIGN KEY (Classrooms) REFERENCES ClassroomsTable (ClassroomsID)
);

create table TeacherTable
(TeacherID int PRIMARY KEY AUTO_INCREMENT,
FirstName VARCHAR(20),
SecondName VARCHAR(20),
ExperienceYears TINYINT UNSIGNED,
Ranks VARCHAR(20)
);

create table ClassroomsTable
(ClassroomsID int PRIMARY KEY AUTO_INCREMENT,
hasComputers Bool,
Adress VARCHAR(50)
);  


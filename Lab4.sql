USE StudentsDataBase;

/*
CREATE INDEX age ON StudentTable (age);
SELECT * FROM StudentTable WHERE age < 19;
*/

#--------------------------------- Неявне з'єднання таблиць предмету та вчителя

SELECT * FROM SubjectTable, TeacherTable WHERE SubjectTable.Teacher = TeacherTable.TeacherID;
SELECT SubjectTable.SubjectName, TeacherTable.FirstName, TeacherTable.SecondName
FROM SubjectTable, TeacherTable
WHERE SubjectTable.Teacher = TeacherTable.TeacherID;

#--------------------------------- Inner Join

SELECT StudentTable.FirstName, StudentTable.SecondName, GroupTable.GroupName
FROM GroupTable
JOIN StudentTable ON StudentTable.GroupID = GroupTable.GroupID;

#--------------------------------- Outer Join
#LEFT: вибірка буде містити всі рядки з першої або лівої таблиці
#RIGHT: вибірка буде містити всі рядки з другої або правої таблиці

SELECT SubjectName, Classrooms, hasComputers
FROM SubjectTable LEFT JOIN ClassroomsTable
ON SubjectTable.Classrooms = ClassroomsTable.ClassroomsID;


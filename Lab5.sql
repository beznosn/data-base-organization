USE StudentsDataBase;

DELIMITER $$

#--------------------------------- процедури 

CREATE PROCEDURE StudentTable_data ()
     SELECT * FROM StudentTable; $$

#--------------------------------- Оператор DECLARE


CREATE PROCEDURE my_procedure_Local_Variables()
BEGIN   /* объявление локальной переменной */   
DECLARE a INT DEFAULT 10;   
DECLARE b, c INT;    /* использование локальной переменной */   
SET a = a + 5;   
SET b = 3;   
SET c = a + b;    

BEGIN      /* локальная переменная во вложенном блоке */      
DECLARE c INT;             
SET c = 5;       
SELECT a, b, c;   
END;    

SELECT a, b, c;
END$$
DELIMITER ;

CALL my_procedure_Local_Variables();


#--------------------------------- Параметр IN приймає число від користувача

DELIMITER $$
CREATE PROCEDURE my_proc_IN (IN var1 INT)
    BEGIN
    SELECT * FROM StudentTable LIMIT var1;
    END $$

    DELIMITER ;

    CALL my_proc_in (2) $$

#--------------------------------- Параметр OUT

DELIMITER $$
CREATE PROCEDURE my_proc_OUT (OUT oldest_student INT)
     BEGIN
     SELECT MAX (age) INTO oldest_student FROM StudentTable;
    -END $$

CALL my_proc_OUT (@M) $$
SELECT @M $$


DELIMITER ;




#--------------------------------- оператор IF

DELIMITER $$


CREATE  PROCEDURE `GetClassroomWithComputersAdress`(INOUT adress VARCHAR(50), IN hasComputers Bool)
BEGIN
DECLARE boolComps Bool;
SELECT hasComputers INTO boolComps FROM ClassroomsTable;
IF boolComps = true 
THEN
	SET adress = Adress;

END IF;


CALL GetClassroomWithComputersAdress(@A, true)$$
SELECT @A; $$

DELIMITER ;


#--------------------------------- оператор LOOP


DELIMITER $$

DELIMITER $$
CREATE PROCEDURE `my_proc_LOOP` (IN num INT)
BEGIN
DECLARE x INT;
SET x = 0;
loop_label: LOOP
	INSERT INTO number VALUES (rand());
	SET x = x + 1;
	IF x >= num 
	THEN
		LEAVE loop_label;
	END IF;
END LOOP;
END$$

CALL my_proc_LOOP(3);
select * from number;


DELIMITER ;

#--------------------------------- оператор REPEAT


DELIMITER $$
CREATE PROCEDURE my_proc_REPEAT (IN n INT)
BEGIN
SET @sum = 0;
SET @x = 1;  
REPEAT   
	IF mod(@x, 2) = 0 
	THEN   
		SET @sum = @sum + @x;   
	END IF;   
	SET @x = @x + 1;   
	UNTIL @x > n 
END REPEAT;
END $$

call my_proc_REPEAT(5);
SELECT @sum;

DELIMITER ;



#--------------------------------- Оператор WHILE 

DELIMITER $$
CREATE PROCEDURE my_proc_WHILE(IN n INT)
BEGIN
SET @sum = 0;
SET @x = 1;
WHILE @x<n 
DO
 	IF mod(@x, 2) <> 0 THEN   
		SET @sum = @sum + @x;   
	END IF;   
SET @x = @x + 1;   
END WHILE;
END$$

CALL my_proc_WHILE(5);

SELECT @sum;


DELIMITER ;

#--------------------------------- Курсори 

DECLARE a, b INT;
DECLARE cur1 CURSOR FOR SELECT value_a, value_b FROM test.t1;
OPEN cur1;

#Далі ви можете вибирати дані з курсора, рядок за рядком.
# поміщаємо наступну пару значень в змінні a і b
FETCH cur1 INTO a, b;
# виконуємо якісь маніпуляції з даними
IF a <b THEN
   ...
ELSE
   ...
END IF;
#поміщаємо наступну пару значень в змінні a і b
FETCH cur1 INTO a, b;
# виконуємо якісь маніпуляції з даними
IF a <b THEN
   ...
ELSE
   ...
END IF;
#Завершується робота з курсором ось так:
CLOSE cur1;

#--------------------------------- TRIGGER

CREATE TABLE test1(a1 INT);
CREATE TABLE test2(a2 INT);
CREATE TABLE test3(a3 INT NOT NULL AUTO_INCREMENT PRIMARY KEY);
CREATE TABLE test4(a4 INT NOT NULL AUTO_INCREMENT PRIMARY KEY,  b4 INT DEFAULT 0);
DELIMITER |
CREATE TRIGGER testref BEFORE INSERT ON test1
 FOR EACH ROW
BEGIN
    INSERT INTO test2 SET a2 = NEW.a1;
    DELETE FROM test3 WHERE a3 = NEW.a1;
    UPDATE test4 SET b4 = b4 + 1 WHERE a4 = NEW.a1;
 END;
|
DELIMITER ;
INSERT INTO test3 (a3) VALUES (NULL), (NULL), (NULL), (NULL), (NULL), (NULL), (NULL), (NULL), (NULL), (NULL);

INSERT INTO test4 (a4) VALUES (0), (0), (0), (0), (0), (0), (0), (0), (0), (0);




